TrolMote : Universal Remote Control!
====================================

TrolMote is a Universal Remote Control that allows one to define custom, arbitrary controls for remotely controlling just about anything that can be controlled via a URI.

Concept:
--------

Using a phone/tablet/pc as a remote control for virtually anything that can expose an API or be controlled via a proxy-API.

Any controllable basically gets defined by: 

- A Uri (any scheme is potential candidate, depending on protocols and control-vector supported by target)
- A name

The corresponding resource (called via the Uri) can optionally return a payload, and if it does, that payload can be parsed and shown to the caller as the respose/status of their action(s). Currently, returned response is presented as content in on the status screen or an error is shown if the action failed (where failure is relative).


The remote control could be used to do just about anything, including (but not limited to):

- detonating weapons
- setting off alarms
- remotely clearing disks
- checking status of remote resources or processes (think ping/ack checks)
- restarting processes, daemons or computers
- turning off/on devices (could be done via proxy-APIs on proxy-controllers e.g infra-red remote controls with tcp interfaces, fridges/toasters with open TCP sockets, etc)
- simply fetch info on arbitrary uris and display it (logs, weather forecasts, latest stock performance, executive summaries, etc)

REAL-WORLD IMPLEMENTATION?
--------------------------

You might think this is a far-fetched idea, but we are already active using this technology, together with a robust, slim, powerful shell-level webserver called **Trol**, that will probably become opensource as well - once we've finished approving that it's safe for placing in the public domain.

For now, one of the ways you can get started using TrolMote yourself is to clone this repository, setup and server (as an normal Django app) and point your browser at it. Login, define control actions and get started using the remote control interface to do just about anything you fancy.

Another quicker approach is for you to get an account on the [trolmote.nuchwezi.com](http://trolmote.nuchwezi.com) universal remote control interface, and setup your own remote control actions to use on your personalized TrolMote interface. For now though, usage of this TrolMote at NuChwezi is limited to only invite-only users, and so you must request the owners of that trolmote (NuChwezi), to grant you access to that TrolMote instance - all serious users /evaluators will readily be granted access.

Check out a sample instance of TrolMote in action here...

![trolmote.png](https://bitbucket.org/repo/ojre8n/images/3420964000-trolmote.png)

Get In-Touch, Contribute!
-------------------------

Fork this project, send patches, feature requests or just ask to share a bike ride with the hackers behind this awesome tool!

Evolve the future of remote controls...