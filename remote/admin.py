from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from remote.models import UserProfile, Account, Controllable
import logging
from trolmote.settings import MAIN_LOG, LOG_LEVEL


logging.basicConfig(level=LOG_LEVEL, filename=MAIN_LOG)
logger = logging.getLogger(__name__)

admin.site.unregister(User)


class UserProfileInline(admin.StackedInline):
    model = UserProfile


class UserProfileAdmin(UserAdmin):
    inlines = [UserProfileInline]
    filter_horizontal = ('groups', 'user_permissions')

    def __init__(self, *args, **kwargs):
        super(UserProfileAdmin, self).__init__(*args, **kwargs)
        self.list_display += ('view_account',)

    def view_account(self, obj):
        try:
            return obj.profile.account
        except:
            return None
    view_account.short_description = 'Account'

    class Meta:
        model = UserProfile

admin.site.register(User, UserProfileAdmin)


class ControllableAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'index', 'name', 'action_class', 'icon_class')
    list_editable = ('index', 'name', 'action_class', 'icon_class')

    def has_delete_permission(self, request, obj=None):
        try:
            return request.user.is_superuser or obj.account.user_profile.user == request.user
        except:
            return request.user.is_superuser


    def queryset(self, request):
        try:
            return request.user.profile.account.my_controllables.all()
        except:
            return Controllable.objects.none()

admin.site.register(Controllable, ControllableAdmin)


class AccountAdmin(admin.ModelAdmin):
    def has_add_permission(self, request):
        return request.user.is_superuser

    def has_delete_permission(self, request, obj=None):
        return request.user.is_superuser

    def queryset(self, request):
        try:
            return Account.objects.filter(id=request.user.profile.account.id)
        except:
            if request.user.is_superuser:
                return super(AccountAdmin, self).queryset(request)
            else:
                return Account.objects.none()

admin.site.register(Account, AccountAdmin)
