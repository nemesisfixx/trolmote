# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Account',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('last_update', models.DateTimeField(auto_now=True)),
                ('tag', models.TextField(null=True, editable=False, blank=True)),
                ('name', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'accounts',
                'verbose_name': 'Account',
                'verbose_name_plural': 'Accounts',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Controllable',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('last_update', models.DateTimeField(auto_now=True)),
                ('tag', models.TextField(null=True, editable=False, blank=True)),
                ('name', models.CharField(max_length=100)),
                ('action_uri', models.URLField()),
                ('action_class', models.CharField(default=b'btn btn-default', help_text=b'Specify a class that will determine how action button looks', max_length=100, choices=[(b'METALLIC', b'btn btn-default')])),
                ('icon_class', models.CharField(help_text=b'Specify any Font-Awesome compatible icon class to use as an icon for the action', max_length=100)),
                ('enabled', models.BooleanField(default=True)),
                ('owner', models.ForeignKey(to='remote.Account')),
            ],
            options={
                'db_table': 'controllables',
                'verbose_name': 'Controllable',
                'verbose_name_plural': 'Controllables',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('last_update', models.DateTimeField(auto_now=True)),
                ('tag', models.TextField(null=True, editable=False, blank=True)),
                ('account', models.OneToOneField(related_name='user_profile', to='remote.Account')),
                ('user', models.OneToOneField(related_name='profile', to=settings.AUTH_USER_MODEL, help_text=b"This profile's affiliated user in the System")),
            ],
            options={
                'db_table': 'user_profiles',
                'verbose_name': 'User Profile',
                'verbose_name_plural': 'User Profiles',
            },
            bases=(models.Model,),
        ),
    ]
