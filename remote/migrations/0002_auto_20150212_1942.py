# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('remote', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='controllable',
            old_name='owner',
            new_name='account',
        ),
    ]
