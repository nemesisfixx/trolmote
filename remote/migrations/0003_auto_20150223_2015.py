# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('remote', '0002_auto_20150212_1942'),
    ]

    operations = [
        migrations.AlterField(
            model_name='controllable',
            name='account',
            field=models.ForeignKey(related_name=b'my_controllables', to='remote.Account'),
        ),
        migrations.AlterField(
            model_name='controllable',
            name='action_class',
            field=models.CharField(default=b'btn btn-lg btn-default', help_text=b'Specify a class that will determine how action button looks', max_length=100, choices=[(b'btn btn-lg btn-default', b'METALLIC'), (b'btn  btn-lg btn-primary', b'WATER'), (b'btn btn-lg btn-success', b'GRASS'), (b'btn btn-lg btn-danger', b'BLOOD'), (b'btn btn-lg btn-warning', b'FIRE'), (b'btn btn-lg btn-coal', b'COAL'), (b'btn btn-lg btn-maroon', b'MAROONED'), (b'btn btn-lg btn-lima', b'LIMA'), (b'btn btn-lg btn-gold', b'GOLD'), (b'btn btn-lg btn-sea', b'SEA'), (b'btn btn-lg btn-black', b'BLACK'), (b'btn btn-lg btn-silver', b'SILVER')]),
        ),
        migrations.AlterField(
            model_name='controllable',
            name='action_uri',
            field=models.CharField(max_length=500),
        ),
    ]
