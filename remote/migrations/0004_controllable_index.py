# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('remote', '0003_auto_20150223_2015'),
    ]

    operations = [
        migrations.AddField(
            model_name='controllable',
            name='index',
            field=models.PositiveIntegerField(default=1),
            preserve_default=True,
        ),
    ]
