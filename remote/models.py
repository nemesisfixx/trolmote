from django.db import models
from django.contrib.auth.models import User


class UserProfile(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    last_update = models.DateTimeField(auto_now=True)
    tag = models.TextField(editable=False, null=True, blank=True)

    user = models.OneToOneField(User, related_name='profile', unique=True, help_text="This profile's affiliated user in the System")
    account = models.OneToOneField('Account', related_name='user_profile', unique=True)

    class Meta:
        db_table = 'user_profiles'
        verbose_name = 'User Profile'
        verbose_name_plural = 'User Profiles'

    def __unicode__(self):
        return "%s (%s)" % (self.user, self.account)


class Account(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    last_update = models.DateTimeField(auto_now=True)
    tag = models.TextField(editable=False, null=True, blank=True)

    name = models.CharField(max_length=100)

    class Meta:
        db_table = 'accounts'
        verbose_name = 'Account'
        verbose_name_plural = 'Accounts'

    def __unicode__(self):
        return self.name


class ACTION_CLASS:
    METALLIC = 'btn btn-lg btn-default'
    WATER = 'btn  btn-lg btn-primary'
    GRASS = 'btn btn-lg btn-success'
    BLOOD = 'btn btn-lg btn-danger'
    FIRE = 'btn btn-lg btn-warning'
    COAL = 'btn btn-lg btn-coal'
    MAROONED = 'btn btn-lg btn-maroon'
    LIMA = 'btn btn-lg btn-lima'
    GOLD = 'btn btn-lg btn-gold'
    SEA = 'btn btn-lg btn-sea'
    BLACK = 'btn btn-lg btn-black'
    SILVER = 'btn btn-lg btn-silver'
    DEFAULT = METALLIC

    CHOICES = (
            (METALLIC, 'METALLIC'),
            (WATER, 'WATER'),
            (GRASS, 'GRASS'),
            (BLOOD, 'BLOOD'),
            (FIRE, 'FIRE'),
            (COAL, 'COAL'),
            (MAROONED, 'MAROONED'),
            (LIMA, 'LIMA'),
            (GOLD, 'GOLD'),
            (SEA, 'SEA'),
            (BLACK, 'BLACK'),
            (SILVER, 'SILVER'),
            )



class Controllable(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    last_update = models.DateTimeField(auto_now=True)
    tag = models.TextField(editable=False, null=True, blank=True)

    account = models.ForeignKey(Account, related_name='my_controllables')
    name = models.CharField(max_length=100)
    index = models.PositiveIntegerField(default=1)
    action_uri = models.CharField(max_length=500)
    action_class = models.CharField(max_length=100, default=ACTION_CLASS.DEFAULT, choices=ACTION_CLASS.CHOICES, help_text='Specify a class that will determine how action button looks')
    icon_class = models.CharField(max_length=100, help_text='Specify any Font-Awesome compatible icon class to use as an icon for the action')
    enabled = models.BooleanField(default=True)

    class Meta:
        ordering = ['index']
        db_table = 'controllables'
        verbose_name = 'Controllable'
        verbose_name_plural = 'Controllables'

    def __unicode__(self):
        return "%s (%s)" % (self.name, 'ON' if self.enabled else 'OFF')


