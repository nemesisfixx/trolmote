(function($) {
    $(document).ready(function() {
        
       var status = $('#status');
       var action_payload = $('#action-payload');

       function remote_control(uri){

              status.attr({'class': 'alert alert-info'});
              action_payload.html( '<i class="fa fa-spinner fa-spin"></i> Processing Action...' );

            $.get(uri, function(data) {
                  status.attr({'class': 'alert alert-success'});
                  action_payload.html( data );
            })
            .fail(function() {
                  status.attr({'class': 'alert alert-danger'});
                  action_payload.html( '<i class="fa fa-close"></i> Action Failed!' );
            });
        }

       function supported_protocol(uri){
           var re = /^https?/i;
           if(re.exec(uri))
               return true;
           else 
               return false;
       }


       function trigger_uri(uri){
           $('#trigger-uri').attr({'href': uri});
           $('#trigger-uri')[0].click();
       }
        
        $('.control').click(function(e){
            var uri = $(this).data('href');
            if(supported_protocol(uri))
                remote_control($(this).data('href'));
            else
                trigger_uri(uri);
        });

    });
}(jQuery));
