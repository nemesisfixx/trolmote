from trolmote.settings import STATIC_URL, APP_NAME, APP_META


def extras(request):
    stuff = {
        'STATIC_URL': STATIC_URL,
        'APP_NAME': APP_NAME,
        'APP_META': APP_META,
    }

    if request.user.is_authenticated():
        try:
            account = request.user.profile.account
            stuff['ACCOUNT'] = account
        except:
            pass

    return stuff
