# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


BASE_URL = ''
# pecially used to construct lots of shareable uris on the platform...
DOMAIN = 'trolmote.nuchwezi.com'
DEBUG = False

import socket
# so we can detect dev from production env and change uris and domains...
SERVER_HOSTNAME = socket.gethostname()
if SERVER_HOSTNAME == 'matrix.nuchwezi':
    DOMAIN = 'localhost'
    BASE_URL = '/trolmote/'
    DEBUG = True


APPEND_SLASH = True
LOGIN_URL = '%sadmin/login/' % BASE_URL
LOGOUT_URL = '%sadmin/logout/' % BASE_URL

TEMPLATE_DEBUG = DEBUG
ALLOWED_HOSTS = ['.%s' % DOMAIN, '.%s.' % DOMAIN, 'localhost', '62.75.194.245']

ADMINS = (
    ('Joe Willrich', 'joewillrich@gmail.com'),
)

MANAGERS = ADMINS

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'vk(!mgihl5z#y7s&6aywk$gqsp3zzs7xex1@=!^u7k4nnm#urt'


# Application definition

INSTALLED_APPS = (
    'django_admin_bootstrapped.bootstrap3',
    'django_admin_bootstrapped',
    'django.contrib.admin',

    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'remote',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'trolmote.urls'

WSGI_APPLICATION = 'trolmote.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases
from trolmote.db_settings import DATABASES


# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '%s/static/' % BASE_URL
STATIC_ROOT = '%s/static/' % BASE_DIR



import errno
from dateutil.easter import datetime
import logging
# - LOGGING ---------------

def make_sure_path_exists(path):
    try:
        os.makedirs(path)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise

LOG_DIR = "%s/logs" % BASE_DIR
make_sure_path_exists(LOG_DIR)
MAIN_LOG = '%s/log_%s' % (LOG_DIR, datetime.date.strftime(datetime.date.today(), '%b_%d_%Y'))
LOG_LEVEL = logging.ERROR

AUTH_PROFILE_MODULE = 'remote.UserProfile'

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.core.context_processors.tz",
    "django.contrib.messages.context_processors.messages",
    'django.core.context_processors.request',

    "trolmote.context_processors.extras",
)


TEMPLATE_DIRS = (
    '%s/templates/' % BASE_DIR,
) + tuple(['%s/%s/templates/' % (BASE_DIR, c) for c in ['trolmote', 'remote', ]])  # project-specific templates


# ---- APP STUFF ---------
APP_NAME = 'TrolMote'
APP_META = {
    'NAME': APP_NAME,
    'AUTHOR': 'Nemesis Fixx',
    'DESCRIPTION': '%s is  is a Universal Remote Control that allows one to define custom, arbitrary controls for remotely controlling just about anything that can be controlled via a URI.' % APP_NAME,
    'VERSION': '1.0',
}


