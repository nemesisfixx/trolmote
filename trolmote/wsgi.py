import os
import sys

base = os.path.dirname(os.path.dirname(__file__))
base_parent = os.path.dirname(base)

# Activate virtual env
activate_env=os.path.join(base, 'env/bin/activate_this.py')
execfile(activate_env, dict(__file__=activate_env))

import site
site.addsitedir(os.path.join(base, 'env/lib/python2.7/site-packages'))

sys.path.append(base)
sys.path.append(base_parent)

os.environ['DJANGO_SETTINGS_MODULE'] = 'trolmote.settings'

from django.core import wsgi
application = wsgi.get_wsgi_application()
